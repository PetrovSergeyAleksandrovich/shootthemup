// Shoot Them Up Game. All rights reserved


#include "UI/STUPlayerStatRowWidget.h"
#include "Components/Image.h"
#include "Components/TextBlock.h"


void USTUPlayerStatRowWidget::SetPlayerName(const FText& Text)
{
	if (!PlayerNameText) return;
	PlayerNameText->SetText(Text);
}

void USTUPlayerStatRowWidget::SetKills(const FText& Text)
{
	if (!KillsText) return;
	KillsText->SetText(Text);
}

void USTUPlayerStatRowWidget::SetDeaths(const FText& Text)
{
	if (!DeathsText) return;
	DeathsText->SetText(Text);
}

void USTUPlayerStatRowWidget::SetTeam(const FText& Text)
{
	if (!TeamText) return;
	TeamText->SetText(Text);
}

void USTUPlayerStatRowWidget::SetPlayerIndicatorVisibility(bool Visible)
{
	if (!PlayerIndicatorImage) return;
	PlayerIndicatorImage->SetVisibility(Visible ? ESlateVisibility::Visible : ESlateVisibility::Hidden);
}

void USTUPlayerStatRowWidget::SetTeamColor(FLinearColor Color)
{
	if (!TeamImage) return;
	TeamImage->SetColorAndOpacity(Color); 
}