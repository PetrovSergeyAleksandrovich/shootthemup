// Shoot Them Up Game. All rights reserved


#include "UI/STUHealthBarWidget.h"
#include "Components/ProgressBar.h"

void USTUHealthBarWidget::SetHealthPercent(float inPercent)
{
	if (!HealthProgressBar) return;

	const auto HealthBarVisibility = (inPercent > PercentVisibilityThreshold || FMath::IsNearlyZero(inPercent)) ? ESlateVisibility::Hidden : ESlateVisibility::Visible;

	HealthProgressBar->SetVisibility(HealthBarVisibility);

	const auto HealthBarColor = inPercent > PercentColorThreshold ? GoodColor : BadColor;
	HealthProgressBar->SetFillColorAndOpacity(HealthBarColor);

	HealthProgressBar->SetPercent(inPercent);
}